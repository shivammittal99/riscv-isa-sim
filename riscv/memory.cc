#include "memory.h"

void MemoryUnit::read(uint64_t src, uint8_t *dst, size_t len) {
	memcpy(dst, &data[src], len);
}

void MemoryUnit::write(uint64_t dst, const uint8_t *src, size_t len) {
	memcpy(&data[dst], src, len);
}

void MemoryUnit::init(uint64_t size) {
	data = (uint8_t *) calloc(1, size);
}

Cache::Cache(const char *config, const char *name) {
	const char* wp = strchr(config, ':');
	if(!wp++) help();
	const char* bp = strchr(wp, ':');
	if(!bp++) help();

	sets = atoi(std::string(config, wp).c_str());
	ways = atoi(std::string(wp, bp).c_str());
	lines = atoi(bp);

	if(lines%8 != 0) help();

	idx_shift = __builtin_popcount(lines - 1);		

	this->name = std::string(name);

	tags = (uint64_t *) calloc(1, sets*ways*sizeof(uint64_t));

	init(sets*ways*lines);
}

void Cache::read(uint64_t src, uint8_t *dst, size_t len) {
	location loc = locate(src);
	if(loc.valid) {
		// Cache hit
		memcpy(dst, get_addr(loc), len);
		return;
	}
	// Cache miss
	location victim = victimize(src);
	memcpy(dst, get_addr(victim), len);
}

void Cache::write(uint64_t dst, const uint8_t *src, size_t len) {
	location loc = locate(dst);
	if(loc.valid) {
		// Cache hit
		memcpy(get_addr(loc), src, len);
		tags[loc.set * ways + loc.way] |= DIRTY;
		writeback(loc.set, loc.way);
		return;
	}
	// Cache miss
	location victim = victimize(dst);
	memcpy(get_addr(victim), src, len);
	tags[victim.set * ways + victim.way] |= DIRTY;
	writeback(victim.set, victim.way);	
}

Cache::location Cache::locate(uint64_t addr) {
	size_t set = (addr >> idx_shift) & (sets-1);
	size_t tag = (addr >> idx_shift) | VALID;
	size_t line = addr & (lines - 1);

	for(size_t i = 0; i < ways; i++) {
		if(tag == (tags[set*ways + i] & ~DIRTY)) {
			return {set, i, line, true};
		}
	}

	return {0, 0, 0, false};
}

Cache::location Cache::victimize(uint64_t addr) {
	size_t set = (addr >> idx_shift) & (sets-1);
	size_t way = lfsr.next() % ways;
	size_t line = addr & (lines - 1);

	uint64_t victim_tag = tags[set * ways + way];

	if((victim_tag & (VALID | DIRTY)) == (VALID | DIRTY)) {
		// Victim is valid and dirty
		// Needs to be written back
		writeback(set, way);
	}

	// Read from higher memory
	tags[set * ways + way] = (addr >> idx_shift) | VALID;
	uint64_t line_addr = addr & ~(lines - 1);
	higher_mem->read(line_addr, get_addr(locate(line_addr)), lines);

	return {set, way, line, true};
}

void Cache::writeback(size_t set, size_t way) {
	uint64_t addr = (tags[set * ways + way] & ~(VALID | DIRTY)) << idx_shift;
	higher_mem->write(addr, get_addr({set, way, 0, true}), lines);
}

uint8_t *Cache::get_addr(location loc) {
	return &data[loc.set * ways * lines + 
				 loc.way * lines +
				 loc.line];
}

void ICache::write(uint64_t dst, const uint8_t *src, size_t len) {
	std::cerr << "Trying to write to ICache" << std::endl;
	exit(1);	
}

PrinceModule::PrinceModule() {
	init(8);
	uint64_t key_high = 0xabcdef0123456789;
	uint64_t key_low = 0x9876543210abcdef;
	prince = Prince_new(key_high, key_low);
}

void PrinceModule::read(uint64_t src, uint8_t *dst, size_t len) {
	uint64_t message;
	for(size_t i = 0; i < len; i += 8) {
		higher_mem->read(src + i, data, 8);
		message = prince_decrypt(&prince, *(uint64_t *)data);
		memcpy(dst + i, &message, 8);
	}
}

void PrinceModule::write(uint64_t dst, const uint8_t *src, size_t len) {
	uint64_t ciphertext;
	for(size_t i = 0; i < len; i += 8) {
		memcpy(data, src + i, 8);
		ciphertext = prince_encrypt(&prince, *(uint64_t *)data);
		higher_mem->write(dst + i, (uint8_t *) &ciphertext, 8);
	}
}
