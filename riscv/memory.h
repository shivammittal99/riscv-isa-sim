#ifndef _RISCV_MEMORY_H
#define _RISCV_MEMORY_H

#include <cstdlib>
#include <string>
#include <iostream>
#include <cstring>
#include "../../prince/prince.h"

class MemoryUnit {
	public:
		MemoryUnit() {}

		MemoryUnit(uint64_t size) {
			init(size);
		}

		MemoryUnit(uint64_t size, uint8_t *data) {
			this->data = data;
		}

		void setHigherMem(MemoryUnit *mem) {
			higher_mem = mem;
		}

		virtual void read(uint64_t src, uint8_t *dst, size_t len);
		virtual void write(uint64_t dst, const uint8_t *src, size_t len);

	protected:
		uint8_t *data;
		MemoryUnit *higher_mem;

		void init(uint64_t size);
};

class Cache : public MemoryUnit {
	public:
		Cache(const char *config, const char *name);

		void read(uint64_t src, uint8_t *dst, size_t len);
		void write(uint64_t dst, const uint8_t *src, size_t len);

	protected:
		static const uint64_t VALID = 1ULL << 63;
		static const uint64_t DIRTY = 1ULL << 62;

		class lfsr_t {
			public:
				lfsr_t() : reg(1) {}
				lfsr_t(const lfsr_t& lfsr) : reg(lfsr.reg) {}
				uint32_t next() { return reg = (reg>>1)^(-(reg&1) & 0xd0000001); }
			private:
				uint32_t reg;
		};

		lfsr_t lfsr;

		size_t sets;
		size_t ways;
		size_t lines;

		size_t idx_shift;

		uint64_t *tags;

		std::string name;

		void help() {
			std::cerr << "Cache configurations must be of the form" << std::endl;
			std::cerr << "  sets:ways:blocksize" << std::endl;
			std::cerr << "where sets, ways, and blocksize are positive integers, with" << std::endl;
			std::cerr << "sets and blocksize both powers of two and blocksize at least 8." << std::endl;
			exit(1);
		}

		struct location {
			size_t set, way, line;
			bool valid;
		};

		location locate(uint64_t addr);
		location victimize(uint64_t addr);
		void writeback(size_t set, size_t way);
		uint8_t *get_addr(location loc);
};

class ICache : public Cache {
	public:
		ICache(const char *config) : Cache(config, "I$") {}

		void write(uint64_t dst, const uint8_t *src, size_t len);
};

class DCache : public Cache {
	public:
		DCache(const char *config) : Cache(config, "D$") {}
};

class PrinceModule : public MemoryUnit {
	public:
		PrinceModule();

		void read(uint64_t src, uint8_t *dst, size_t len);
		void write(uint64_t dst, const uint8_t *src, size_t len);
	
	protected:
		Prince prince;
};

#endif
